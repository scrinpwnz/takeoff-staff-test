import React, {CSSProperties} from 'react'
import {useSpring, config} from 'react-spring'
import {Diff} from "utility-types";

export type WithAnimate3DCardType<T> = T & Animate3DCardType

type Animate3DCardType = {
    animatedStyle: CSSProperties
}

export const withAnimate3DCardClass = <BaseProps extends Animate3DCardType>(WrappedComponent: React.ComponentType<BaseProps>, params = {power: 40, scale: 1}) => {

    type HocProps = Diff<BaseProps, Animate3DCardType>

    return (props: HocProps) => {

        const [springProps, setSpringProps] = useSpring(() => ({
            xys: [0, 0, 1],
            config: config.default
        }))

        const clamp = (min: number, max: number, val: number) => Math.min(Math.max(min, val), max)

        const animate3DCardCalc = (x: number, y: number, ref: HTMLDivElement) => {
            let e = ref.getBoundingClientRect()
            let newY = -((y - e.top) - e.height / 2) / (e.height / params.power)
            let newX = ((x - e.left) - e.width / 2) / (e.width / params.power)
            newY = clamp(-5, 5, newY)
            newX = clamp(-5, 5, newX)
            return [newY, newX, params.scale]
        }
//
        const animate3DCardInterpolator = (x: number, y: number, s: number) => `perspective(800px) rotateX(${x*4}deg) rotateY(${y}deg) scale(${s})`

        // @ts-ignore
        let animatedStyle = {transform: springProps.xys.interpolate(animate3DCardInterpolator)}

        const ref = React.createRef<HTMLDivElement>()

        const {...restProps} = props

        return (
            <div ref={ref}
                 style={{width: '100%', height: '100%'}}
                 onMouseMove={(event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
                     setSpringProps({xys: animate3DCardCalc(event.clientX, event.clientY, ref.current!)})
                 }}
                 onMouseLeave={() => setSpringProps({xys: [0, 0, 1]})}>
                <WrappedComponent {...(restProps as BaseProps)} animatedStyle={animatedStyle}/>
            </div>
        )
    }
}

export const withAnimate3DCardTarget = <BaseProps extends Animate3DCardType>(WrappedComponent: React.ComponentType<BaseProps>) => {

    type HocProps = Diff<BaseProps, Animate3DCardType>

    return (props: HocProps) => {

        const {...restProps} = props

        return (
                <WrappedComponent {...(restProps as BaseProps)}/>
        )
    }
}





