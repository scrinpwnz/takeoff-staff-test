import React, {ChangeEvent, useEffect, useState} from 'react'
import {makeStyles, Theme} from "@material-ui/core/styles";
import {Box, Button, Divider, IconButton, Paper, TextField} from "@material-ui/core";
import {AppDispatch, RootState} from "../../redux/store";
import {connect} from "react-redux";
import {getContacts, setSearchValue} from "../../redux/slices/ContactsSlice";
import ContactsItem from "./ContactsItem";
import ReactWindowTable from "../../helpers/ReactWindowTable";
import Title from "../Title";
import AddIcon from '@material-ui/icons/Add';
import AddContactModal from "./AddContactModal";
import {withAnimate3DCardClass, WithAnimate3DCardType} from "../../helpers/Animate3DCard";
import {animated} from 'react-spring';

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        display: 'grid',
        width: '100%',
        height: '100%',
        gridTemplateAreas: `
        'contacts title'
        `,
        gridTemplateColumns: '500px 1fr',
        gridGap: theme.spacing(2),
        padding: theme.spacing(2),
        [theme.breakpoints.down('md')]: {
            gridTemplateAreas: `
            'title'
            'contacts'
            `,
            gridTemplateColumns: '500px',
            gridTemplateRows: '150px 1fr'
        },
    },
    contacts: {
        gridArea: 'contacts',
    },
    title: {
        gridArea: 'title',
        height: '100%',
        display: 'grid',
        placeItems: 'center',
    },
    paper: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column'
    },
    autoSizer: {
        flex: '1',
        overflow: 'hidden',
        position: 'relative',
        "& ::-webkit-scrollbar": {
            width: theme.spacing(1),
        },
        "& ::-webkit-scrollbar-thumb": {
            background: theme.palette.primary.main
        },
        "& ::-webkit-scrollbar-track": {
            background: theme.palette.background.default
        },
        "& > div > div:focus": {
            outline: "none"
        }
    },
    box: {
        width: 'auto',
        padding: theme.spacing(2),
        '& > *': {
            marginRight: theme.spacing(2),
        },
        '& > *:last-child': {
            marginRight: theme.spacing(0),
        },
    },
}))

type ContactsPropsType = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>

const ContactsPage: React.FC<WithAnimate3DCardType<ContactsPropsType>> = props => {

    const classes = useStyles()

    const [modalState, setModalState] = useState<boolean>(false)

    const {contacts, getContacts, setSearchValue, searchValue} = props

    useEffect(() => getContacts(), [])

    const filteredData = contacts.filter(item => {
        let search = searchValue.toLowerCase()
        return (item.name.toLowerCase().includes(search) || item.phone.includes(search))
    })

    const contactsData = filteredData.sort((a, b) => {
        if (a.id < b.id) return 1
        if (a.id > b.id) return -1
        return 0
    }).map(item => <ContactsItem contact={item}/>)

    const handleSearch = (e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => {
        setSearchValue(e.target.value)
    }
    const handleClearButton = () => setSearchValue('')
    const handleOpenModal = () => setModalState(true)
    const handleCloseModal = () => setModalState(false)

    return (
        <div className={classes.root}>
            <div className={classes.contacts}>
                <Paper elevation={3} className={classes.paper}>
                    <Box alignItems="center" display='flex' className={classes.box}>
                        <TextField id="outlined-basic"
                                   color={"secondary"}
                                   label="Поиск"
                                   variant="outlined"
                                   onChange={handleSearch}
                                   value={searchValue}
                        />
                        <div style={{display: 'flex', flex: 1}}>
                            <div style={{display: 'flex', flexGrow: 1, alignItems: 'center'}}>
                                <Button onClick={handleClearButton}
                                        variant={"contained"} color={"secondary"}>
                                    Очистить
                                </Button>
                            </div>
                            <div>
                                <IconButton onClick={handleOpenModal}>
                                    <AddIcon/>
                                </IconButton>
                            </div>
                        </div>
                    </Box>
                    <Divider/>
                    <div className={classes.autoSizer}>
                        <ReactWindowTable data={contactsData} itemSize={72} dataExists={contacts.length > 0}/>
                    </div>
                </Paper>
                <AddContactModal state={modalState} handleClose={handleCloseModal}/>
            </div>
            <div className={classes.title}>
                <animated.div style={props.animatedStyle}>
                    <Title>Контакты</Title>
                </animated.div>
            </div>
        </div>
    )
}

const mapStateToProps = (state: RootState) => {
    return {
        contacts: state.Contacts.contacts,
        searchValue: state.Contacts.searchValue,
    }
}

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        getContacts: () => {
            dispatch(getContacts())
        },
        setSearchValue: (value: string) => {
            dispatch(setSearchValue(value))
        },
    }
}

const AnimatedContactsPage = withAnimate3DCardClass(ContactsPage, {power: 20, scale: 1})

export default connect(mapStateToProps, mapDispatchToProps)(AnimatedContactsPage)
