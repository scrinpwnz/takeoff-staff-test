import React from 'react'
import {makeStyles, Theme} from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'column'
    },
    text: {
        fontSize: '6rem',
        background: `linear-gradient(${theme.palette.primary.main}, ${theme.palette.secondary.main})`,
        '-webkit-background-clip': 'text',
        '-webkit-text-fill-color': 'transparent',
        animation: '$gradient 10s infinite linear'
    },
    '@keyframes gradient': {
        from: {filter: 'hue-rotate(0deg)'},
        to: {filter: 'hue-rotate(360deg)'}
    }
}))

const Title: React.FC = props => {

    const classes = useStyles()

    return (
        <div className={classes.root}>
            <span className={classes.text}>
                {props.children}
            </span>
        </div>
    )
}

export default Title

