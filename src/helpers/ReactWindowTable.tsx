import React, {CSSProperties, useEffect, useState} from 'react'
import {AutoSizer, Size} from "react-virtualized";
import {FixedSizeList} from "react-window";
import {Skeleton} from "@material-ui/lab";
import {animated, useTransition} from 'react-spring'
import {makeStyles, Theme} from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) => ({
    notFound: {
        display: 'flex',
        justifyContent: 'center',
        marginTop: theme.spacing(4)
    },
    notFoundText: {
        ...theme.typography.h4
    }
}))

type ReactWindowTablePropsType = {
    data: Array<any>
    itemSize: number
    dataExists: boolean
}

type RowType = {
    data: any
    index: number
    style: CSSProperties
}

const Row = ({data, index, style}: RowType) => (
    <div key={index} style={style}>{data[index]}</div>
)

const ReactWindowTable: React.FC<ReactWindowTablePropsType> = props => {

    const classes = useStyles()

    const skeletons = (height: number) => {
        let skeletons = []
        for (let i = 0; i < Math.round(height / props.itemSize); i++) {
            skeletons.push(
                <div key={i} style={{paddingLeft: '16px', paddingRight: '16px'}}>
                    <Skeleton animation="wave" height={props.itemSize}/>
                </div>
            )
        }
        return skeletons
    }

    const listSkeleton: React.ReactElement = (
        <AutoSizer>
            {({height, width}: Size) => (
                <div style={{height: height, width: width}}>
                    {skeletons(height)}
                </div>
            )}
        </AutoSizer>
    )

    const list: React.ReactElement = (
        <AutoSizer>
            {({height, width}: Size) => (
                <FixedSizeList height={height}
                               width={width}
                               itemCount={props.data.length}
                               itemSize={props.itemSize}
                               itemData={props.data}>
                    {Row}
                </FixedSizeList>
            )}
        </AutoSizer>
    )

    const notFound: React.ReactElement = (
        <div className={classes.notFound}>
            <span className={classes.notFoundText}>
                <span role={'img'} aria-label={'Not Found'}>🧐 Пусто!</span>
            </span>
        </div>
    )

    const [index, setIndex] = useState(0)

    useEffect(() => {
        if (props.dataExists) {
            (props.data.length === 0) ? setIndex(2) : setIndex(1)
        }
    }, [props.dataExists, props.data.length])

    const items = [
        {id: 0, value: listSkeleton},
        {id: 1, value: list},
        {id: 2, value: notFound}
    ]

    const transitions = useTransition(items[index], item => item.id, {
        from: {position: 'absolute', opacity: 0},
        enter: {opacity: 1},
        leave: {opacity: 0}
    })

    return (
        <>
            {transitions.map(({item, props, key}) =>
                <animated.div key={key} style={{width: '100%', height: '100%', ...props}}>
                    {item.value}
                </animated.div>
            )}
        </>
    )
}

export default ReactWindowTable