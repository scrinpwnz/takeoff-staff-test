import React, {useState} from 'react'
import {makeStyles, Theme} from "@material-ui/core/styles";
import {animated, useTransition} from 'react-spring'
import {connect} from "react-redux";
import {AppDispatch, RootState} from "../redux/store";
import {Alert, AlertTitle} from '@material-ui/lab';
import {Paper} from "@material-ui/core";
import {MessageItemType, setRead} from '../redux/slices/MessageHubSlice';
import {getFormattedDate} from "../helpers/helpers";

const useStyles = makeStyles((theme: Theme) => ({
    main: {
        position: 'fixed',
        display: 'flex',
        flexDirection: 'column-reverse',
        right: theme.spacing(3),
        bottom: theme.spacing(3),
        zIndex: 10000
    },
    messageText: {
        fontSize: '24px'
    }
}))

type MessagePropsType = {
    item: MessageItemType,
    setRead: () => void
}

const Message: React.FC<MessagePropsType> = props => {

    const {item, setRead} = props

    const titles = {
        error: 'Ошибка!',
        warning: 'Внимание!',
        info: 'Информация',
        success: 'Успех!'
    }

    const time = getFormattedDate(item.timestamp)

    return (
        <div style={{paddingTop: '8px', width: '300px',}}>
            <Paper elevation={1}>
                <Alert variant={"filled"} severity={item.type} onClose={setRead}>
                    <AlertTitle>{titles[item.type]}</AlertTitle>
                    <p>
                        <strong>{`${time.time}: `}</strong>
                        {(item.status) ? item.status + '' : ''}{` ${item.statusText}`}
                    </p>
                </Alert>
            </Paper>
        </div>
    )
}

type MyMessageHubPropsType = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>

const MessageHub: React.FC<MyMessageHubPropsType> = props => {

    const classes = useStyles()

    const [refMap] = useState(() => new WeakMap())

    const {items, setRead} = props

    const filteredItems = items.filter(item => !item.read)

    const transitions = useTransition(filteredItems, item => item.id, {
        from: {opacity: 0, transform: 'translate3d(-500%,-1000%, 0) scale(3)', height: 0},
        // @ts-ignore
        enter: item => async next => {
            if (refMap.has(item)) {
                await next({
                    opacity: 1,
                    height: refMap.get(item).offsetHeight,
                    transform: 'translate3d(0%, 0%, 0) scale(1)'
                })
            }
        },
        leave: [
            {transform: 'translate3d(200%, 0%, 0) scale(1)'},
            {opacity: 0, height: 0},
        ]
    })

    return (
        <div className={classes.main}>
            {transitions.map(
                // @ts-ignore
                ({key, item, props}) => {
                    return (
                        <animated.div key={key} style={props}>
                            <div ref={ref => ref && refMap.set(item, ref)}>
                                <Message item={item} setRead={() => setRead(item.id)}/>
                            </div>
                        </animated.div>
                    )
                })}
        </div>
    )
}

const mapStateToProps = (state: RootState) => {
    return {
        items: state.MessageHub.items
    }
}

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        setRead: (id: number) => {
            dispatch(setRead(id))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageHub)

