import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {AppThunk} from "../store";
import {sleep} from "../../helpers/helpers";

const initialState = {
    token: null as string | null,
    rememberMe: null as boolean | null
}

const Session = createSlice({
    name: 'Session',
    initialState,
    reducers: {
        setSession: (state, action: PayloadAction<{ token: string, rememberMe: boolean }>) => {
            localStorage.setItem('token', action.payload.token)
            localStorage.setItem('rememberMe', action.payload.rememberMe.toString())
            state.token = action.payload.token
            state.rememberMe = action.payload.rememberMe
        },
        setToken: (state, action: PayloadAction<string>) => {
            localStorage.setItem('token', action.payload)
            state.token = action.payload
        },
        clearSession: state => {
            localStorage.setItem('token', '')
            localStorage.removeItem('rememberMe')
            return initialState
        },
    },
})

export const checkAuth = (): AppThunk => async dispatch => {
    await sleep(0).then(() => {
        if (localStorage.getItem('rememberMe') === 'true') {
            const token = localStorage.getItem('token')
            if (token !== null) dispatch(setToken(token))
            else dispatch(setToken(''))
        } else dispatch(setToken(''))
    }) // таймаут для имитации задержки ответа от сервера
}

export const {
    setToken, setSession, clearSession
} = Session.actions

export default Session.reducer