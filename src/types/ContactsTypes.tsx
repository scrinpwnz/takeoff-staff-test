import {ContactsItemType} from "./APITypes";

export type ContactStateType = {
    selected: boolean
    deleting: StatusStateType
    editing: StatusStateType & { editingMode: boolean }
}

export type StatusStateType = {
    success: boolean
    inProgress: boolean
}

export type AddContactStateType = {
    error: {
        name: string | null
        phone: string | null
    }
} & StatusStateType


export type ContactType = ContactsItemType & { state: ContactStateType }


export type FindContactType = {
    (data: Array<ContactType>, contact: ContactType): ContactType
}

