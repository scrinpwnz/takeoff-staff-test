import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {AppThunk} from "../store";
import {API} from "../../api/api";
import {errorHandler} from "./MessageHubSlice";
import {AddContactPayloadType, ContactsItemType, EditContactPayloadType} from "../../types/APITypes";
import {sleep} from "../../helpers/helpers";
import {inProgress} from "./AppSlice";
import {
    AddContactStateType,
    ContactStateType,
    ContactType,
    FindContactType,
    StatusStateType
} from "../../types/ContactsTypes";


const contactStatusState = {
    success: false,
    inProgress: false
} as StatusStateType

const contactState = {
    selected: false,
    deleting: {...contactStatusState},
    editing: {...contactStatusState, editingMode: false},
} as ContactStateType

const initialState = {
    contacts: [] as Array<ContactType>,
    searchValue: '' as string,
    addingState: {...contactStatusState, error: {
        name: null,
        phone: null,
    }} as AddContactStateType
}

const findContact: FindContactType = (data, row) => {
    return data.find(item => item.id === row.id)!
}

const Contacts = createSlice({
    name: 'Contacts',
    initialState,
    reducers: {
        resetContactsSlice: state => initialState,
        setContacts: (state, action: PayloadAction<Array<ContactsItemType>>) => {
            state.contacts = action.payload.map(item => ({...item, state: {...contactState}}))
        },
        setSearchValue: (state, action: PayloadAction<string>) => {
            state.searchValue = action.payload
        },
        setContactSelected: (state, action: PayloadAction<{ contact: ContactType, selected: boolean }>) => {
            findContact(state.contacts, action.payload.contact).state.selected = action.payload.selected
        },
        deleteContact: (state, action: PayloadAction<{ contact: ContactType }>) => {
            state.contacts = state.contacts.filter(item => item.id !== action.payload.contact.id)
        },
        setContactDeleting: (state, action: PayloadAction<{ contact: ContactType, inProgress: boolean }>) => {
            findContact(state.contacts, action.payload.contact).state.deleting.inProgress = action.payload.inProgress
        },
        setContactDeleted: (state, action: PayloadAction<{ contact: ContactType, success: boolean }>) => {
            findContact(state.contacts, action.payload.contact).state.deleting.success = action.payload.success
        },
        setContactEditingMode: (state, action: PayloadAction<{ contact: ContactType, editingMode: boolean }>) => {
            findContact(state.contacts, action.payload.contact).state.editing.editingMode = action.payload.editingMode
        },
        setContactEditing: (state, action: PayloadAction<{ contact: ContactType, inProgress: boolean }>) => {
            findContact(state.contacts, action.payload.contact).state.editing.inProgress = action.payload.inProgress
        },
        setContactEdited: (state, action: PayloadAction<{ contact: ContactType, success: boolean }>) => {
            findContact(state.contacts, action.payload.contact).state.editing.success = action.payload.success
        },
        editContact: (state, action: PayloadAction<{ contact: ContactType, payload: EditContactPayloadType }>) => {
            let contact = findContact(state.contacts, action.payload.contact)
            contact.state.selected = false
            contact.name = action.payload.payload.name
            contact.phone = action.payload.payload.phone
            contact.state = {...contactState}
        },
        addContact: (state, action: PayloadAction<ContactsItemType>) => {
            state.contacts.push({...action.payload, state: {...contactState}})
        },
        setContactAdding: (state, action: PayloadAction<boolean>) => {
            state.addingState.inProgress = action.payload
        },
        setContactAdded: (state, action: PayloadAction<boolean>) => {
            state.addingState.success = action.payload
        },
        setContactAddingNameError: (state, action: PayloadAction<string>) => {
            state.addingState.error.name = action.payload
        },
        setContactAddingPhoneError: (state, action: PayloadAction<string>) => {
            state.addingState.error.phone = action.payload
        },
    }
})


export const getContacts = (): AppThunk => async dispatch => {
    try {
        dispatch(inProgress(true))
        const result = await API.getContacts()
        await sleep(500).then(() => {
            if (result) dispatch(setContacts(result)) // имитация задержки ответа
        })
    } catch (error) {
        dispatch(errorHandler(error))
    } finally {
        dispatch(inProgress(false))
    }
}

export const deleteContactFromDatabase = (contact: ContactType): AppThunk => async dispatch => {
    try {
        dispatch(setContactDeleting({contact, inProgress: true}))
        await API.deleteContact(contact.id)
        await sleep(500)
        dispatch(setContactDeleted({contact, success: true}))
        dispatch(setContactDeleting({contact, inProgress: false}))
        sleep(1000).then(() => {
            dispatch(deleteContact({contact}))
        })
    } catch (error) {
        dispatch(errorHandler(error))
    } finally {
        dispatch(setContactDeleting({contact, inProgress: false}))
    }
}

export const editContactInDatabase = (contact: ContactType, payload: EditContactPayloadType): AppThunk => {
    return (async dispatch => {
        try {
            dispatch(setContactEditing({contact, inProgress: true}))
            await API.editContact(contact.id, payload)
            await sleep(500)
            dispatch(setContactEdited({contact, success: true}))
            dispatch(setContactEditing({contact, inProgress: false}))
            sleep(1000).then(() => {
                dispatch(editContact({contact, payload}))
                dispatch(setContactEditingMode({contact, editingMode: false}))
            })
        } catch (error) {
            dispatch(errorHandler(error))
        } finally {
            dispatch(setContactEditing({contact, inProgress: false}))
        }
    })
}

export const addContactToDatabase = (payload: AddContactPayloadType): AppThunk => {
    return (async dispatch => {
        try {
            dispatch(setContactAdding(true))
            const result = await API.addContact(payload)
            await sleep(500)
            dispatch(setContactAdded(true))
            dispatch(setContactAdding(false))
            if (result) {
                sleep(1000).then(() => {
                    dispatch(addContact(result))
                    dispatch(setContactAdded(false))
                })
            }
        } catch (error) {
            dispatch(errorHandler(error))
        } finally {
            dispatch(setContactAdding(false))
        }
    })
}

export const {
    resetContactsSlice, setContacts, setSearchValue, setContactSelected,
    deleteContact, setContactDeleting, setContactDeleted, setContactEditingMode,
    setContactEditing, setContactEdited, editContact, addContact, setContactAdded,
    setContactAdding, setContactAddingNameError, setContactAddingPhoneError
} = Contacts.actions

export default Contacts.reducer