import React from 'react'
import {Theme} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import Logo from '../../logo.svg'
import Title from "../Title";
import {withAnimate3DCardClass, WithAnimate3DCardType} from "../../helpers/Animate3DCard";
import { animated } from 'react-spring';

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        height: '100%',
        display: 'grid',
        placeItems: 'center',
        padding: theme.spacing(2)
    },
}))

type MainPageTypeProps = WithAnimate3DCardType<{}>

const MainPage: React.FC<MainPageTypeProps> = props => {

    const classes = useStyles()

    return (
        <div className={classes.root}>
            <animated.div style={props.animatedStyle}>
                <Title>Главная страница</Title>
            </animated.div>
        </div>
    )
}

export default withAnimate3DCardClass(MainPage, {power: 20, scale: 1})

