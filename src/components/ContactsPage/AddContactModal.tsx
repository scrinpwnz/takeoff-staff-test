import React, {useRef} from 'react'
import {
    AppBar,
    Backdrop,
    Button,
    CircularProgress,
    Fade,
    IconButton,
    Modal,
    Paper,
    TextField,
    Theme,
    Toolbar,
    Typography
} from "@material-ui/core";
import CloseIcon from '@material-ui/icons/Close';
import {makeStyles} from "@material-ui/core/styles";
import NumberFormat from 'react-number-format';
import {AppDispatch, RootState} from "../../redux/store";
import {
    addContactToDatabase,
    setContactAddingNameError,
    setContactAddingPhoneError
} from "../../redux/slices/ContactsSlice";
import {AddContactPayloadType, ContactsItemType} from "../../types/APITypes";
import {connect} from "react-redux";
import CheckIcon from '@material-ui/icons/Check';
import {sleep} from "../../helpers/helpers";

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        outline: 'none',
        width: '400px',
        position: 'absolute',
        left: 'calc(50% - 200px)',
        top: 'calc(50% - 40vh)',
        paddingBottom: theme.spacing(2),
        overflow: 'hidden',
        [theme.breakpoints.down('xs')]: {
            transform: 'scale(0.7)'
        },
        "& > div > div:focus": {
            outline: "none"
        }
    },
    closeIcon: {
        color: 'inherit'
    },
    addingWrapper: {
        display: 'grid',
        gridGap: theme.spacing(1.5),
        padding: theme.spacing(2)
    }
}))

type AddContactModalPropsType = {
    state: boolean,
    handleClose: () => void
} & ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>

const AddContactModal: React.FC<AddContactModalPropsType> = props => {

    const classes = useStyles()

    const {state, handleClose, addingState, addContact, setContactAddingNameError, setContactAddingPhoneError} = props

    const nameRef = useRef<HTMLInputElement>(null)
    const phoneRef = useRef<HTMLInputElement>(null)

    const validateName = (value: string) => {
        if (!value) {
            setContactAddingNameError('Поле не может быть пустым')
            return
        }
        setContactAddingNameError('')
    }

    const validatePhone = (value: string) => {
        if (!value) {
            setContactAddingPhoneError('Поле не может быть пустым')
            return
        }
        if (value.replace(/[^0-9]/g, '').length < 10) {
            setContactAddingPhoneError('Неправильынй формат номера')
            return;
        }
        setContactAddingPhoneError('')
    }

    const handleCloseModal = () => {
        sleep(300).then(() => {
            setContactAddingNameError('')
            setContactAddingPhoneError('')
        })
        handleClose()
    }


    const handleAddButton = () => {
        if (nameRef.current && phoneRef.current) {
            const status = ['online', 'away', 'offline'] as Array<ContactsItemType['status']>
            const payload = {
                name: nameRef.current.value,
                phone: phoneRef.current.value,
                status: status[Math.floor(Math.random() * status.length)]
            }
            if (payload.name && payload.phone) {
                addContact(payload)
                sleep(1500).then(handleClose)
            } else {
                validateName(payload.name)
                validatePhone(payload.phone)
            }
        }
    }

    return (
        <Modal open={state}
               onClose={handleCloseModal}
               closeAfterTransition
               BackdropComponent={Backdrop}
               BackdropProps={{
                   timeout: 500,
               }}
        >
            <Fade in={state}>
                <Paper className={classes.root}>
                    <AppBar position={'relative'}>
                        <Toolbar variant={'dense'}>
                            <Typography variant="h6" style={{flexGrow: 1}}>
                                Добавление контакта
                            </Typography>
                            <IconButton edge="end" onClick={handleCloseModal} className={classes.closeIcon}>
                                <CloseIcon/>
                            </IconButton>
                        </Toolbar>
                    </AppBar>
                    <div className={classes.addingWrapper}>
                        <TextField
                            inputProps={{
                                maxLength: 30,
                            }}
                            inputRef={nameRef}
                            helperText={addingState.error.name}
                            error={!!(addingState.error.name)}
                            label={'Имя'}/>
                        <NumberFormat customInput={TextField}
                                      format="+7 (###) #######"
                                      inputRef={phoneRef}
                                      helperText={addingState.error.phone}
                                      error={!!(addingState.error.phone)}
                                      label={'Телефон'}
                        />
                        <Button variant={'contained'}
                                color={'secondary'}
                                onClick={handleAddButton}
                                disabled={addingState.inProgress || addingState.success}
                        >
                            {addingState.inProgress
                                ? <CircularProgress size={24} color={'inherit'}/>
                                : (addingState.success)
                                    ? <CheckIcon/>
                                    : 'Добавить'}
                        </Button>
                    </div>
                </Paper>
            </Fade>
        </Modal>
    )
}

const mapStateToProps = (state: RootState) => {
    return {
        addingState: state.Contacts.addingState
    }
}

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        addContact: (payload: AddContactPayloadType) => {
            dispatch(addContactToDatabase(payload))
        },
        setContactAddingNameError: (error: string) => {
            dispatch(setContactAddingNameError(error))
        },
        setContactAddingPhoneError: (error: string) => {
            dispatch(setContactAddingPhoneError(error))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddContactModal)