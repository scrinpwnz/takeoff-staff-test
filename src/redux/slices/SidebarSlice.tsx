import {createSlice} from "@reduxjs/toolkit"
import {SidebarItem} from "../../types/SidebarTypes";

const initialState = {
    tabs: [
        {id: 1, label: 'Главная', value: '/', icon: 'dashboard'},
        {id: 2, label: 'Контакты', value: '/contacts', icon: 'contacts'},
    ] as Array<SidebarItem>,
    open: false as boolean,
}

const Sidebar = createSlice({
    name: 'Sidebar',
    initialState,
    reducers: {
        toggle: state => {
            state.open = !state.open
        },
    },
})

export const {toggle} = Sidebar.actions

export default Sidebar.reducer