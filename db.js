const Faker = require('faker')

module.exports = () => {
    const data = {
        users: [
            {
                "id": 1,
                "name": "User",
                "email": "user@gmail.com",
                "password": "$2a$10$bkQYGQb2UOCGpGV6ym5kEuedLuJ4BeMhOKC2CIozq92FMeB0bJzlm"
            }
        ],
        contacts: []
    }

    const status = ['online', 'away', 'offline']

    for (let i = 0; i < 1000; i++) {
        data.contacts.push({
            id: i,
            name: `${Faker.name.firstName()} ${Faker.name.lastName()}`,
            phone: Faker.phone.phoneNumber('79#########'),
            status: status[Math.floor(Math.random() * status.length)]
        })
    }
    return data
}