export type LoginResponseType = {
    accessToken: string
}

export type ContactsItemType = {
    id: number
    name: string
    phone: string
    status: 'online' | 'away' | 'offline'
}

export type EditContactPayloadType = Pick<ContactsItemType, 'name' | 'phone'>

export type AddContactPayloadType = Omit<ContactsItemType, 'id'>

export type EditedContactsItemType = EditContactPayloadType & Pick<ContactsItemType, 'id'>
