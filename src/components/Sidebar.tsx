import React from 'react'
import {withRouter} from 'react-router-dom'
import {connect} from "react-redux"
import {AppDispatch, RootState} from "../redux/store"
import {toggle} from "../redux/slices/SidebarSlice"
import clsx from "clsx";
import IconButton from "@material-ui/core/IconButton";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Drawer from "@material-ui/core/Drawer";
import {makeStyles, Theme} from "@material-ui/core/styles";
import {RouteComponentProps} from "react-router";
import DashboardIcon from '@material-ui/icons/Dashboard';
import ContactsIcon from '@material-ui/icons/Contacts';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import CheckIcon from '@material-ui/icons/Check';
import {logout} from "../redux/slices/LoginSlice";
import {CircularProgress} from "@material-ui/core";

type iconsType = {
    [key: string]: React.ReactElement
}

const icons: iconsType = {
    dashboard: <DashboardIcon/>,
    contacts: <ContactsIcon/>,
}

const useStyles = makeStyles((theme: Theme) => ({
    toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(1, .5)
    },
    drawerPaper: {
        position: 'relative',
        width: 240,
        padding: 0,
        background: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.easeInOut,
            duration: theme.transitions.duration.standard,
        }),
        overflow: 'hidden'
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        width: theme.spacing(8)
    },
    tabsIndicator: {
        width: theme.spacing(.5)
    },
    tabRoot: {
        whiteSpace: 'nowrap',
        minWidth: '160px'
    },
    tabWrapperClose: {
        flexDirection: 'row',
        justifyContent: 'left',
    },
    tabIconClose: {
        '& :first-child': {
            minWidth: theme.spacing(5),
            marginRight: theme.spacing(2),
            marginBottom: 0
        }
    },
    logout: {
        display: 'flex',
        flex: '1',
        flexDirection: 'column-reverse',
        alignItems: 'center',
        paddingBottom: theme.spacing(4),
    },
    iconButton: {
        color: 'inherit'
    },
    fabLoading: {
        position: 'absolute',
        left: '0',
        top: '0',
        zIndex: 1,
        color: theme.palette.secondary.main,
    },
}))

type Props = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps> & RouteComponentProps<any>

const Sidebar: React.FC<Props> = props => {

    const classes = useStyles()

    const {open, tabs, toggle, logout, token, logoutInProgress} = props

    const handleCallToRouter = (e: React.ChangeEvent<{}>, value: string) => {
        props.history.push(value)
    }

    const tabClasses = {
        classes: {
            wrapper: clsx(!open && classes.tabWrapperClose),
            labelIcon: clsx(!open && classes.tabIconClose),
            root: classes.tabRoot
        }
    }

    const formalizedTabs = tabs.map(tab =>
        <Tab key={tab.id} icon={icons[tab.icon]} label={tab.label} value={tab.value} {...tabClasses}/>
    )

    return (
        <Drawer
            variant={"permanent"}
            classes={{
                paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
            }}
            open={open}
        >
            <div className={classes.toolbarIcon}>
                <IconButton onClick={toggle}
                            classes={{root: classes.iconButton}}>
                    {open ? <ChevronLeftIcon/> : <ChevronRightIcon/>}
                </IconButton>
            </div>
            <Tabs
                orientation={"vertical"}
                value={props.history.location.pathname}
                onChange={handleCallToRouter}
                classes={{
                    indicator: classes.tabsIndicator,
                }}
            >
                {formalizedTabs}
            </Tabs>
            <div className={classes.logout}>
                <div style={{position: 'relative'}}>
                    <IconButton onClick={logout}
                                classes={{root: classes.iconButton}}>
                        {(token) ? <ExitToAppIcon/> : <CheckIcon/>}
                    </IconButton>
                    {logoutInProgress && <CircularProgress size={48} className={classes.fabLoading}/>}
                </div>
            </div>
        </Drawer>
    )
}

const mapStateToProps = (state: RootState) => {
    return {
        tabs: state.Sidebar.tabs,
        open: state.Sidebar.open,
        token: state.Session.token,
        logoutInProgress: state.Login.logoutInProgress
    }
}

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        toggle: () => dispatch(toggle()),
        logout: () => dispatch(logout()),
    }
}

const withRouterSidebar = withRouter(Sidebar)
export default connect(mapStateToProps, mapDispatchToProps)(withRouterSidebar)














