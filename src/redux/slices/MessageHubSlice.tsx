import {createSlice, PayloadAction} from "@reduxjs/toolkit"
import {AppThunk} from "../store";

export type MessageItemType = {
    id: number
    type: 'error' | 'warning' | 'info' | 'success'
    statusText: string
    status: number | null
    read: boolean
    timestamp: number
}

type AddErrorType = Omit<MessageItemType, 'id' | 'type' | 'read' | 'timestamp'>
type AddWarningType = Omit<MessageItemType, 'id' | 'type' | 'read' | 'timestamp' | 'status'>

const initialState = {
    items: [] as Array<MessageItemType>,
    id: 0 as number
}

const MessageHub = createSlice({
    name: 'MessageHub',
    initialState,
    reducers: {
        resetMessageHub: state => initialState,
        setRead: (state, action: PayloadAction<number>) => {
            state.items = state.items.map(item => {
                if (item.id === action.payload) {
                    return {...item, read: true}
                } else return item
            })
        },
        addError: (state, action: PayloadAction<AddErrorType>) => {
            state.items = [...state.items, {
                id: state.id,
                type: 'error',
                statusText: action.payload.statusText,
                status: action.payload.status,
                read: false,
                timestamp: (new Date()).getTime()
            }]
            state.id = state.id + 1
        },
        addWarning: (state, action: PayloadAction<AddWarningType>) => {
            state.items = [...state.items, {
                id: state.id,
                type: 'warning',
                statusText: action.payload.statusText,
                status: 0,
                read: false,
                timestamp: (new Date()).getTime()
            }]
            state.id = state.id + 1
        },
    },
})


export const errorHandler = (error: any): AppThunk => async dispatch => {
    if (error.response) {
        dispatch(addError({
            status: error.response.status,
            statusText: error.response.statusText
        }))
    } else {
        dispatch(addError({
            status: null,
            statusText: error.message
        }))
    }
}

export const {
    resetMessageHub, setRead, addError, addWarning
} = MessageHub.actions

export default MessageHub.reducer