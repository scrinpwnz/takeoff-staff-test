import React, {useEffect, useState} from 'react'
import Container from "@material-ui/core/Container";
import {createMuiTheme, CssBaseline} from "@material-ui/core";
import {makeStyles, Theme} from "@material-ui/core/styles";
import {AppDispatch, RootState} from "./redux/store";
import {connect} from "react-redux";
import {Route, Switch, useLocation} from 'react-router-dom'
import {animated, useTransition} from 'react-spring'
import FirstLoadPlaceholder from "./components/FirstLoadPlaceholder";
import Login from "./components/LoginPage/LoginPage";
import MainPage from "./components/MainPage/MainPage";
import Contacts from "./components/ContactsPage/ContactsPage";
import {checkAuth} from './redux/slices/SessionSlice';
import MessageHub from "./components/MessageHub";
import ProgressBar from "./components/ProgressBar";
import Sidebar from "./components/Sidebar";
import {ThemeProvider} from "@material-ui/styles";


const useStyles = makeStyles((theme: Theme) => ({
    root: {
        display: 'grid',
        gridTemplateColumns: 'fit-content(240px) minmax(640px, 1fr)',
        minHeight: '100vh',
        overflow: 'auto'
    },
    content: {
        overflowX: 'hidden',
    },
    container: {
        position: 'relative',
        height: '100%',
        padding: 0,
    },
    linearProgress: {
        position: 'absolute',
        width: '100%',
        display: 'none',
        zIndex: 10000
    },
    linearProgressActive: {
        display: 'block'
    }
}))

type AppPropsType = ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>

const App: React.FC<AppPropsType> = props => {

    const classes = useStyles()

    const location = useLocation()

    const {checkAuth, token} = props

    const transitions = useTransition(location, location => location.pathname, {
        from: {opacity: 0, transform: 'translate3d(50%,0,0)'},
        enter: {opacity: 1, transform: 'translate3d(0,0,0)'},
        leave: {opacity: 0, transform: 'translate3d(-100%,0,0)'},
    })

    const mainApp: React.ReactElement = (
        <div className={classes.root}>
            <Sidebar/>
            <main className={classes.content}>
                <Container maxWidth="lg" className={classes.container}>
                    {transitions.map(({item, props, key}) => {
                        return (
                            <animated.div key={key}
                                          style={{position: 'absolute', width: '100%', height: '100%', ...props}}>
                                <Switch location={item}>
                                    <Route exact path={'/'} component={MainPage}/>
                                    <Route path={'/contacts'} component={Contacts}/>
                                </Switch>
                            </animated.div>
                        )
                    })}
                </Container>
            </main>
        </div>
    )

    const items = {
        firstLoadPlaceHolder: {id: 0, value: <FirstLoadPlaceholder/>},
        login: {id: 1, value: <Login/>},
        mainApp: {id: 2, value: mainApp},
    }

    const [index, setIndex] = useState<keyof typeof items>('firstLoadPlaceHolder')

    useEffect(() => {
        if (token === null) checkAuth()
        else if (token === '') setIndex('login')
        else setIndex('mainApp')
    }, [token])

    const authTransitions = useTransition(items[index], item => item.id, {
        from: {position: 'absolute', opacity: 0},
        enter: {opacity: 1},
        leave: {opacity: 0}
    })

    const theme = {
        breakpoints: {
            values: {
                xs: 599,
                sm: 600,
                md: 900,
                lg: 1200,
                xl: 1800
            }
        },
    }

    return (
        <ThemeProvider theme={createMuiTheme(theme)}>
            <CssBaseline/>
            <ProgressBar/>
            <MessageHub/>
            {authTransitions.map(({item, props, key}) =>
                <animated.div key={key} style={{width: '100%', height: '100%', ...props}}>
                    {item.value}
                </animated.div>
            )}
        </ThemeProvider>
    )
}

const mapStateToProps = (state: RootState) => {
    return {
        token: state.Session.token
    }
}

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        checkAuth: () => dispatch(checkAuth())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
