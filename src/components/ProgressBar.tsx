import React from 'react'
import TopBarProgress from "react-topbar-progress-indicator"
import {connect} from 'react-redux'
import useTheme from "@material-ui/core/styles/useTheme";
import {RootState} from "../redux/store";

type Props = ReturnType<typeof mapStateToProps>

const ProgressBar: React.FC<Props> = props => {

    const theme = useTheme()

    TopBarProgress.config({
        barColors: {
            "0": theme.palette.secondary.light,
            "0.5": theme.palette.secondary.main,
            "1.0": theme.palette.secondary.dark,
        },
        shadowBlur: 1,
        barThickness: 2
    })

    const {inProgress} = props

    if (inProgress) {
        return (<TopBarProgress/>)
    } else {
        return (<></>)
    }
}

const mapStateToProps = (state: RootState) => {
    return {
        inProgress: state.App.inProgress
    }
}

export default connect(mapStateToProps)(ProgressBar)