export type SidebarItem = {
    id: number
    label: string
    value: string
    icon: string
}
