import React, {useRef} from 'react'
import {fade, makeStyles, Theme, useTheme} from "@material-ui/core/styles";
import {Avatar, Badge, CircularProgress, IconButton, TextField, Typography} from "@material-ui/core";
import {stringToColor} from "../../helpers/helpers";
import {connect} from "react-redux";
import {AppDispatch, RootState} from "../../redux/store";
import Highlighter from "../../helpers/Highlighter";
import {
    deleteContactFromDatabase,
    editContactInDatabase,
    setContactEditingMode,
    setContactSelected
} from '../../redux/slices/ContactsSlice';
import {ContactType} from "../../types/ContactsTypes";
import clsx from 'clsx'
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import {EditContactPayloadType} from "../../types/APITypes";
import NumberFormat from "react-number-format";

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        display: 'flex',
        height: '72px',
        paddingLeft: theme.spacing(2),
        alignItems: 'center',
        borderBottom: `1px solid #f2f2f2`,
        boxSizing: 'border-box',
        cursor: 'pointer'
    },
    hover: {
        "& > :hover": {
            background: fade(theme.palette.secondary.main, theme.palette.action.selectedOpacity)
        }
    },
    badge: {
        display: 'grid',
        placeItems: 'center'
    },
    avatar: {
        width: theme.spacing(7),
        height: theme.spacing(7),
    },
    info: {
        display: 'flex',
        flex: '1',
        flexDirection: 'column',
        justifyContent: 'center',
        paddingLeft: theme.spacing(2)
    },
    online: {
        '& > span': {
            background: '#43b581'
        }
    },
    away: {
        '& > span': {
            background: '#faa61a'
        }
    },
    offline: {
        '& > span': {
            background: '#727d8a'
        }
    },
    selected: {
        background: fade(theme.palette.secondary.main, theme.palette.action.selectedOpacity)
    },
    clickContainer: {
        flex: '1',
        display: 'flex',
        height: '100%',
        padding: theme.spacing(0, 2),
    },
    buttons: {
        overflow: 'hidden',
        pointerEvents: 'none',
        padding: theme.spacing(0, 2),
        display: 'flex',
        position: 'relative',
        opacity: 0,
        width: '0px',
        maxWidth: 'fit-content',
        transition: theme.transitions.create(['opacity', 'width'], {
            easing: theme.transitions.easing.easeInOut,
            duration: 300,
        })
    },
    buttonsActive: {
        pointerEvents: 'auto',
        opacity: 1,
        width: '128px',
    },
    fabLoading: {
        position: 'absolute',
        right: '0px',
        top: '0px',
        zIndex: 1,
        color: theme.palette.secondary.main
    },
}))

type ContactsItemPropsType = {
    contact: ContactType
} & ReturnType<typeof mapStateToProps> & ReturnType<typeof mapDispatchToProps>

const ContactsItem: React.FC<ContactsItemPropsType> = props => {

    const classes = useStyles()

    const theme = useTheme()

    const {contact, searchValue, setContactSelected, deleteContact, setContactEditingMode, editContact} = props

    const nameRef = useRef<HTMLInputElement>(null)
    const phoneRef = useRef<HTMLInputElement>(null)

    const blocked = (
        contact.state.editing.success ||
        contact.state.editing.inProgress ||
        contact.state.deleting.inProgress ||
        contact.state.deleting.success
    )

    const avatarColor = stringToColor(contact.name)
    const avatarStyle = {
        color: theme.palette.getContrastText(avatarColor),
        background: avatarColor
    }

    const handleClick = () => {
        if (!(blocked || contact.state.editing.editingMode)) setContactSelected(contact, !contact.state.selected)
    }

    const handleEditButton = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.stopPropagation()
        if (!blocked) setContactEditingMode(contact, true)
    }

    const handleDeleteButton = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.stopPropagation()
        if (!blocked) deleteContact(contact)
    }

    const handleCheckEditButton = () => {
        let payload = {
            name: nameRef.current!.value,
            phone: phoneRef.current!.value
        }
        if (!(contact.name === payload.name && contact.phone === payload.phone)) {
            editContact(contact, payload)
        } else {
            setContactSelected(contact, false)
            setContactEditingMode(contact, false)
        }
    }

    const handleCancelEditButton = () => {
        setContactSelected(contact, false)
        setContactEditingMode(contact, false)
    }

    const buttons = (
        <>
            <IconButton onClick={handleEditButton} disabled={blocked}><EditIcon/></IconButton>
            <IconButton onClick={handleDeleteButton} disabled={blocked} style={{position: 'relative'}}>
                {contact.state.deleting.success ? <CheckIcon/> : <DeleteIcon/>}
                {contact.state.deleting.inProgress && <CircularProgress size={48} className={classes.fabLoading}/>}
            </IconButton>
        </>
    )

    const buttonsEditingMode = (
        <>
            <IconButton onClick={handleCheckEditButton} disabled={blocked}>
                {contact.state.editing.success ? <DoneAllIcon/> : <CheckIcon/>}
                {contact.state.editing.inProgress && <CircularProgress size={48} className={classes.fabLoading}/>}
            </IconButton>
            <IconButton onClick={handleCancelEditButton} disabled={blocked}>
                <CloseIcon/>
            </IconButton>
        </>
    )

    const info = (
        <>
            <Typography variant={'body1'}>
                <Highlighter value={searchValue}>{contact.name}</Highlighter>
            </Typography>
            <Typography variant={'body2'}>
                <NumberFormat displayType={'text'}
                              value={contact.phone}
                              format={'+7 (###) #######'}
                />
            </Typography>
        </>
    )

    const infoEditingMode = (
        <>
            <TextField
                inputProps={{
                    maxLength: 30,
                }}
                defaultValue={contact.name}
                inputRef={nameRef}
                disabled={blocked}/>
            <NumberFormat customInput={TextField}
                          format={'+7 (###) #######'}
                          defaultValue={contact.phone}
                          inputRef={phoneRef}
                          disabled={blocked}
            />
        </>
    )

    return (
        <div className={clsx(classes.hover, contact.state.selected && classes.selected)}>
            <div className={classes.root} onClick={handleClick}>
                <Badge className={classes[contact.status]}
                       badgeContent={' '}
                       overlap={'circle'}
                       color={'primary'}
                       anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}
                >
                    <Avatar className={classes.avatar}
                            style={{...avatarStyle}}>{contact.name.charAt(0)}</Avatar>
                </Badge>
                <div className={classes.info}>
                    {(contact.state.editing.editingMode) ? infoEditingMode : info}
                </div>
                <div className={clsx(classes.buttons, contact.state.selected && classes.buttonsActive)}>
                    {(contact.state.editing.editingMode) ? buttonsEditingMode : buttons}
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = (state: RootState) => {
    return {
        searchValue: state.Contacts.searchValue
    }
}

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        setContactSelected: (contact: ContactType, selected: boolean) => {
            dispatch(setContactSelected({contact, selected}))
        },
        deleteContact: (contact: ContactType) => {
            dispatch(deleteContactFromDatabase(contact))
        },
        setContactEditingMode: (contact: ContactType, editingMode: boolean) => {
            dispatch(setContactEditingMode({contact, editingMode}))
        },
        editContact: (contact: ContactType, payload: EditContactPayloadType) => {
            dispatch(editContactInDatabase(contact, payload))
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactsItem)