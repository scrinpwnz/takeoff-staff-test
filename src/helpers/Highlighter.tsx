import React from "react";
import {escapeSpecialCharacters} from "./helpers";
import {makeStyles, Theme} from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) => ({
    highlight: {
        background: theme.palette.primary.main,
        color: theme.palette.primary.contrastText,
    }
}))

type Props = {
    value: string,
    children?: any
}

const Highlighter: React.FC<Props> = (props) => {

    const classes = useStyles()

    if (props.value === '') {
        return (<>{props.children}</>)
    }

    const regexp = new RegExp(`(${escapeSpecialCharacters(props.value)})`, 'i')

    const split = props.children.split(regexp)

    const result = split.map((item: string, index: number) => {
        if (regexp.test(item)) {
            return <span key={index} className={classes.highlight}>{item}</span>
        }
        return <span key={index}>{item}</span>
    })

    return (<>{result}</>)
}

export default Highlighter