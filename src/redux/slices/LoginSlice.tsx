import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {LoginPayloadType} from "../../types/LoginTypes";
import {AppThunk} from "../store";
import {API} from "../../api/api";
import {setSession, clearSession} from "./SessionSlice";
import { inProgress, resetAppSlice } from "./AppSlice";
import {addError} from "./MessageHubSlice";
import { resetContactsSlice } from "./ContactsSlice";
import {sleep} from "../../helpers/helpers";

const initialState = {
    email: '' as string,
    password: '' as string,
    rememberMe: false as boolean,
    passwordHelperText: '' as string,
    signInInProgress: false as boolean,
    logoutInProgress: false as boolean,
    isAuthenticated: false as boolean,
    serverIsNotAvailable: false
}

const Login = createSlice({
    name: 'Login',
    initialState,
    reducers: {
        resetLoginSlice: state => initialState,
        setEmail: (state, action: PayloadAction<string>) => {
            state.email = action.payload
        },
        setPassword: (state, action: PayloadAction<string>) => {
            state.password = action.payload
        },
        toggleRememberMe: (state) => {
            state.rememberMe = !state.rememberMe
        },
        setError: (state, action: PayloadAction<string | null>) => {
            if (action.payload) {
                state.passwordHelperText = action.payload
            } else {
                state.passwordHelperText = ''
            }
        },
        setSignInInProgress: (state, action: PayloadAction<boolean>) => {
            state.signInInProgress = action.payload
        },
        setLogoutInProgress: (state, action: PayloadAction<boolean>) => {
            state.logoutInProgress = action.payload
        },
        setIsAuthenticated: (state, action: PayloadAction<boolean>) => {
            state.isAuthenticated = action.payload
        },
    },
})

export const login = (payload: LoginPayloadType): AppThunk => async dispatch => {
    try {
        dispatch(setSignInInProgress(true))
        dispatch(inProgress(true))
        const result = await API.login(payload)
        if (result) {
            dispatch(setError(null))
            dispatch(setIsAuthenticated(true))
            setTimeout(() => {
                dispatch(setSession({token: result.accessToken, rememberMe: payload.rememberMe}))
            }, 300)
        }
    } catch (error) {
        dispatch(loginErrorHandler(error))
    } finally {
        dispatch(setSignInInProgress(false))
        dispatch(inProgress(false))
    }
}

const loginErrorHandler = (error: any): AppThunk => async dispatch => {
    if (!error.response) {
        dispatch(addError({
            status: null,
            statusText: "Сервер недоступен"
        }))
    } else if (error.response.status === 400) {
        dispatch(setError('Неправильный логин или пароль.'))
    }
}

export const logout = (): AppThunk => async dispatch => {
    dispatch(setLogoutInProgress(true))
    setTimeout(() => {
        dispatch(resetLoginSlice())
        dispatch(clearSession())
        sleep(1000).then(() => dispatch(resetContactsSlice()))
        sleep(1000).then(() => dispatch(resetAppSlice()))
        dispatch(setLogoutInProgress(false))
    }, 500) // имитация задержки ответа
}


export const {
    resetLoginSlice, setEmail, setPassword, toggleRememberMe,
    setError, setSignInInProgress, setLogoutInProgress,
    setIsAuthenticated
} = Login.actions

export default Login.reducer