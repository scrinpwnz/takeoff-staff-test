import axios from 'axios'
import store from "../redux/store";
import {LoginPayloadType} from "../types/LoginTypes";
import {
    AddContactPayloadType,
    ContactsItemType,
    EditContactPayloadType,
    EditedContactsItemType,
    LoginResponseType
} from "../types/APITypes";

const instance = axios.create({
    baseURL: "http://localhost:3001/",
})

instance.interceptors.request.use(config => {
    config.headers.Authorization = `Bearer ${store.getState().Session.token}`
    return config;
})

export const API = {
    login: (payload: LoginPayloadType) => {
        return instance.post('login', {
            email: payload.email,
            password: payload.password
        }).then(response => response.data as LoginResponseType)
    },
    getContacts: () => {
        return instance.get(`contacts`).then(response => response.data as Array<ContactsItemType>)
    },
    deleteContact: (id: number) => {
        return instance.delete(`contacts/${id}`).then(() => true)
    },
    editContact: (id: number, payload: EditContactPayloadType) => {
        return instance.put(`contacts/${id}`, {
            name: payload.name,
            phone: payload.phone
        }).then(response => response.data as EditedContactsItemType)
    },
    addContact: (payload: AddContactPayloadType) => {
        return instance.post(`contacts`, {
            name: payload.name,
            phone: payload.phone,
            status: payload.status
        }).then(response => response.data as ContactsItemType)
    },
};




