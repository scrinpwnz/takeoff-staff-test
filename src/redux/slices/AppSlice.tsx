import {createSlice, PayloadAction} from "@reduxjs/toolkit";

const initialState = {
    inProgress: false as boolean,
}

const App = createSlice({
    name: 'App',
    initialState,
    reducers: {
        resetAppSlice: state => initialState,
        inProgress: (state, action: PayloadAction<boolean>) => {
            state.inProgress = action.payload
        },
    },
})

export const {
    inProgress, resetAppSlice
} = App.actions

export default App.reducer